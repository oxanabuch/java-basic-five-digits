import java.util.Scanner;

public class FiveDigits {
    public static void main(String[] args) {

        double[] myArray;
        int n;
        Scanner in = new Scanner(System.in);
        System.out.print("Enter number of digits : "); // вводимо кількість цифр
        n = in.nextInt();
        myArray = new double[n];

        for (int i = 0; i < n; i++) {
            System.out.print("Enter digit[" + i + "] = "); // вводимо цифри
            myArray[i] = in.nextInt();
        }

        double sum = 0;
        double averageValue;

        // знаходимо суму цифр
        for (double i : myArray) {
            sum += i;
        }
        System.out.println("Sum is " + sum);

        // знаходимо середнє значення усіх цифр
        averageValue = sum / myArray.length;
        System.out.println("Average value is " + averageValue);
    }
}
